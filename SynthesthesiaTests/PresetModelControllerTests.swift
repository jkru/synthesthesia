//
//  PresetModelControllerTests.swift
//  SynthesthesiaTests
//
//  Created by Janek Krukowski on 05/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import XCTest
@testable import Synthesthesia

class PresetModelControllerTests: XCTestCase {
    
    var controller: PresetModelController!
    
    override func setUp() {
        super.setUp()
        controller = PresetModelController(configuration: Configuration.realm(inMemoryIdentifier: "PresetModelControllerTests"))
    }
    
    func testFetchEmpty() {
        let presets = controller.fetchAll()
        XCTAssertTrue(presets.isEmpty)
    }
    
    func testInsertFetch() {
        let preset = Preset(version: 0,
                            name: "test",
                            description: nil,
                            sporth: "test",
                            controls: [])
        controller.insert(preset)
        XCTAssertTrue(controller.fetchAll().count == 1)
    }
}

