//
//  AppFileControllerTests.swift
//  SynthesthesiaTests
//
//  Created by Krukowski, Jan on 3/5/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import XCTest
@testable import Synthesthesia

class AppFileControllerTests: XCTestCase {

    var controller: AppFileController!

    override func setUp() {
        super.setUp()
        controller = AppFileController(configuration: Configuration.realm(inMemoryIdentifier: "AppFileControllerTests"))
    }

    func testFetchEmpty() {
        let recordings = controller.fetchRecordings()
        XCTAssertTrue(recordings.isEmpty)
    }

    func testInsertFetch() {
        controller.save(path: "test", presetName: "test")
        let recordings = controller.fetchRecordings()
        XCTAssertTrue(recordings.count == 1)
    }

    func testInsertFetchDelete() {
        controller.save(path: "test", presetName: "test")
        let recording = controller.fetchRecordings().first!
        controller.delete(recording: recording)
        XCTAssertTrue(controller.fetchRecordings().isEmpty)
    }

    func testInsertFetchRename() {
        controller.save(path: "test", presetName: "test")
        let recording = controller.fetchRecordings().first!
        let newName = "newtest"
        controller.rename(recording: recording, newName: newName)
        XCTAssertTrue(controller.fetchRecordings().first!.name == newName)
        XCTAssertFalse(FileManager.default.fileExists(atPath: recording.path))
    }
}
