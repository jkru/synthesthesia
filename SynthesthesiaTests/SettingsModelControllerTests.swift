//
//  SettingsModelControllerTests.swift
//  SynthesthesiaTests
//
//  Created by Krukowski, Jan on 3/6/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import XCTest
@testable import Synthesthesia

class SettingsModelControllerTests: XCTestCase {

    var controller: SettingsModelController!

    override func setUp() {
        super.setUp()
        controller = SettingsModelController(configuration: Configuration.realm(inMemoryIdentifier: "SettingsModelControllerTests"))
    }

    func testFetch() {
        let settings = controller.fetch()
        XCTAssertTrue(settings.plotType == .none)
    }

    func testUpdateFetch() {
        controller.update(plotType: .waveform)
        let settings = controller.fetch()
        XCTAssertTrue(settings.plotType == .waveform)
    }
}
