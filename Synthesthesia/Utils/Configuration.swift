//
//  Configuration.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/2/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

struct Configuration {
    static func defaultRealm() -> Realm.Configuration {
        return realm(fileName: Constants.Realm.local)
    }

    static func realm(fileName: String) -> Realm.Configuration {
        let fileURL = applicationSupportURL(for: fileName)
        return Realm.Configuration(fileURL: fileURL,
                                   inMemoryIdentifier: nil,
                                   syncConfiguration: nil,
                                   encryptionKey: nil,
                                   readOnly: false,
                                   schemaVersion: Constants.Realm.schemaVersion,
                                   migrationBlock: nil,
                                   deleteRealmIfMigrationNeeded: false,
                                   objectTypes: Constants.Realm.objectTypes)
    }

    static func realm(inMemoryIdentifier: String) -> Realm.Configuration {
        let fileURL = applicationSupportURL(for: Constants.Realm.local)
        return Realm.Configuration(fileURL: fileURL,
                                   inMemoryIdentifier: inMemoryIdentifier,
                                   syncConfiguration: nil,
                                   encryptionKey: nil,
                                   readOnly: false,
                                   schemaVersion: Constants.Realm.schemaVersion,
                                   migrationBlock: nil,
                                   deleteRealmIfMigrationNeeded: false,
                                   objectTypes: Constants.Realm.objectTypes)
    }

    static private func applicationSupportURL(for fileName: String) -> URL? {
        let path = try? FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fullPath = path?.appendingPathComponent(fileName)
        copyIfNeeded(to: fullPath)
        return fullPath
    }
    
    static private func copyIfNeeded(to path: URL?) {
        guard let path = path else {
            return
        }
        if FileManager.default.fileExists(atPath: path.path) {
            return
        }
        guard let source = Bundle.main.url(forResource: "Synthesthesia", withExtension: "realm") else {
            return
        }
        try? FileManager.default.copyItem(at: source, to: path)
    }
}
