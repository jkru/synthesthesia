
//
//  Extensions.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/14/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import FontAwesome_swift
import AudioKitUI

extension UITabBarController {
    static var `default`: UITabBarController {
        let navigationController1 = UINavigationController()
        navigationController1.navigationBar.prefersLargeTitles = true
        navigationController1.viewControllers = [PresetsViewController.instantiate()]
        let image1 = UIImage.fontAwesomeIcon(name: .music, textColor: .black, size: .tabBarImageSize)
        navigationController1.tabBarItem = UITabBarItem(title: nil, image: image1, tag: 0)
        navigationController1.tabBarItem.imageInsets = .tabBarInsets
        let navigationController2 = UINavigationController()
        navigationController2.navigationBar.prefersLargeTitles = true
        navigationController2.viewControllers = [RecordingsViewController.instantiate()]
        let image2 = UIImage.fontAwesomeIcon(name: .listOL, textColor: .black, size: .tabBarImageSize)
        navigationController2.tabBarItem = UITabBarItem(title: nil, image: image2, tag: 0)
        navigationController2.tabBarItem.imageInsets = .tabBarInsets
        let navigationController3 = UINavigationController()
        navigationController3.navigationBar.prefersLargeTitles = true
        navigationController3.viewControllers = [SettingsViewController.instantiate()]
        let image3 = UIImage.fontAwesomeIcon(name: .cog, textColor: .black, size: .tabBarImageSize)
        navigationController3.tabBarItem = UITabBarItem(title: nil, image: image3, tag: 0)
        navigationController3.tabBarItem.imageInsets = .tabBarInsets
        let tabbar = UITabBarController()
        tabbar.viewControllers = [navigationController1, navigationController2, navigationController3]
        return tabbar
    }
}

extension UIAlertController {
    static func renameAlertController(name: String, callback: @escaping (String) -> Void) -> UIAlertController {
        let controller = UIAlertController(title: "Rename recording", message: nil, preferredStyle: .alert)
        controller.addTextField { textField in
            textField.text = name
            textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak controller] _ in
            if let newName = controller?.textFields?.first?.text {
                callback(newName)
            } else {
                callback(name)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(okAction)
        controller.addAction(cancelAction)
        return controller
    }
}

extension UIEdgeInsets {
    static let tabBarInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
}

extension CGSize {
    static let tabBarImageSize = CGSize(width: 30, height: 30)
    static let playImageSize = CGSize(width: 24, height: 24)
}

extension UIViewController {
    static func load<T: UIViewController>(from storyboard: Storyboard, with identifier: ViewControllerId) -> T {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: Bundle.main)
        guard let vc = storyboard.instantiateViewController(withIdentifier: identifier.rawValue) as? T else {
            fatalError("Fatal error when instantiating \(identifier) from storyboard \(storyboard).")
        }
        return vc
    }
}

extension UITableView {
    func cell<T: UITableViewCell>(id: TableViewCell, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: id.rawValue, for: indexPath) as! T
    }
}

extension Date {
    var ticks: UInt64 {
        return UInt64(timeIntervalSince1970 * 10_000_000)
    }
    
    func format() -> String {
        return Constants.dateFormatter.string(from: self)
    }
}

extension TimeInterval {
    static func statusBarAnimationTime(_ isHidden: Bool) -> TimeInterval {
        if isHidden {
            return 0.15
        }
        return 0.25
    }

    static let buttonAnimationTime: TimeInterval = 0.5
}

extension UIView {
    static func plotView(inView view: UIView) -> UIView {
        let plotView = AKOutputWaveformPlot.createView()
        let yval = view.frame.size.height / 2
        plotView.frame = CGRect(x: 0, y: yval, width: view.frame.size.width, height: plotView.frame.size.height)
        return plotView
    }
}

extension UIButton {
    func circle(borderWidth: CGFloat = 3) {
        layer.borderWidth = borderWidth
        layer.cornerRadius = min(frame.width, frame.height) / 2
    }
}

extension UIToolbar {
    func transparent() {
        setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default)
        setShadowImage(UIImage(), forToolbarPosition: .any)
    }
}

extension Bundle {
    var version: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
}
