//
//  Constants.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/2/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

struct Constants {
    struct Realm {
        static let local = "Synthesthesia.realm"
        static let schemaVersion: UInt64 = 0
        static let objectTypes = [PresetObject.self, ControlObject.self, RecordingObject.self, SettingsObject.self]
    }
    
    static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .medium
        return formatter
    }()

    static let appAuthor = "Jan Krukowski"
}
