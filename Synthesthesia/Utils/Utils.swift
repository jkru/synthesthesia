//
//  Utils.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/16/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

enum Storyboard: String {
    case camera = "Camera"
    case presets = "Presets"
    case preview = "Preview"
    case recordings = "Recordings"
    case settings = "Settings"
}

enum ViewControllerId: String {
    case camera = "CameraViewController"
    case presets = "PresetsViewController"
    case preview = "PreviewViewController"
    case recordings = "RecordingsViewController"
    case settings = "SettingsViewController"
}

enum TableViewCell: String {
    case recording = "RecordingTableViewCell"
    case preset = "PresetTableViewCell"
    case plotType = "PlotTypeTableViewCell"
    case appInfo = "AppInfoTableViewCell"
}
