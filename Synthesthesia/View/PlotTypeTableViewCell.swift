//
//  PlotTypeTableViewCell.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 05/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import UIKit

final class PlotTypeTableViewCell: UITableViewCell {
    private var plotType: PlotType = .none
    @IBOutlet var plotSegment: UISegmentedControl!
    var didTapPlotType: (PlotType) -> Void = { _ in }
    
    func configure(with plotType: PlotType) {
        self.plotType = plotType
        plotSegment.selectedSegmentIndex = plotType.rawValue
    }
    
    @IBAction func didTapSegment(_ sender: Any) {
        guard let plotType = PlotType(rawValue: plotSegment.selectedSegmentIndex) else {
            return
        }
        self.plotType = plotType
        didTapPlotType(plotType)
    }
}
