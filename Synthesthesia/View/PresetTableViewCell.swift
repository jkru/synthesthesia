//
//  PresetTableViewCell.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 05/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import UIKit

final class PresetTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    func configure(with preset: Preset) {
        nameLabel.text = preset.name
        descriptionLabel.text = preset.description
    }
}
