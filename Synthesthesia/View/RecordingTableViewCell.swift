//
//  RecordingTableViewCell.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 03/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import FontAwesome_swift
import UIKit

final class RecordingTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var presetLabel: UILabel!
    @IBOutlet var playButton: UIButton!
    private let playImage = UIImage.fontAwesomeIcon(name: .play, textColor: .black, size: .playImageSize)
    private let pauseImage = UIImage.fontAwesomeIcon(name: .pause, textColor: .black, size: .playImageSize)
    private var isPlaying = false {
        didSet {
            if isPlaying {
                playButton.setImage(pauseImage, for: .normal)
            } else {
                playButton.setImage(playImage, for: .normal)
            }
        }
    }
    private var recording: Recording?
    var didTap: (Bool, Recording) -> Void = { _, _ in }
    
    func configure(with recording: Recording, isPlaying: Bool) {
        self.recording = recording
        self.isPlaying = isPlaying
        nameLabel.text = recording.name
        dateLabel.text = "Date: \(recording.date.format())"
        presetLabel.text = "Preset: \(recording.presetName)"
    }

    @IBAction func didTapPlay(_ sender: Any) {
        isPlaying = !isPlaying
        if let recording = self.recording {
            didTap(isPlaying, recording)
        }
    }
}
