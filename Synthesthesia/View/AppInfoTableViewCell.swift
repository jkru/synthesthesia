//
//  AppInfoTableViewCell.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/9/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import UIKit

final class AppInfoTableViewCell: UITableViewCell {
    @IBOutlet var versionLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        if let version = Bundle.main.version {
            versionLabel.text = "Version: \(version)"
        }
        authorLabel.text = "Author: \(Constants.appAuthor)"
    }
}
