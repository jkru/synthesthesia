//
//  TensorflowController.h
//  Synthesthesia
//
//  Created by Krukowski, Jan on 1/31/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface TensorflowController: NSObject

- (void)initalize;
- (void)runModelOnFrame:(CVPixelBufferRef _Nonnull)frame callback:(nonnull void(^)(NSDictionary* _Nonnull))callback;

@end
