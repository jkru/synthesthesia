//
//  CameraController.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 1/31/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Photos

final class CameraController: NSObject {
    private let session = AVCaptureSession()
    private let movieOutput = AVCaptureMovieFileOutput()
    private let queue = DispatchQueue(label: "CameraManager")
    var handler: ((CVImageBuffer) -> Void)?

    override init() {
        super.init()
        if UIDevice.current.userInterfaceIdiom == .phone {
            session.sessionPreset = .vga640x480
        } else {
            session.sessionPreset = .photo
        }
    }

    func start(for cameraRootView: UIView) throws {
        try setupInputDevice()
        setupOutputDevice()
        let previewLayer = getPreviewLayer()
        let rootLayer = cameraRootView.layer
        rootLayer.masksToBounds = true
        previewLayer.frame = rootLayer.bounds
        rootLayer.insertSublayer(previewLayer, at: 0)
        previewLayer.frame = cameraRootView.frame
        session.startRunning()
    }

    func stop() {
        session.stopRunning()
    }

    private func getPreviewLayer() -> AVCaptureVideoPreviewLayer {
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        return previewLayer
    }

    private func setupInputDevice() throws {
        guard let device = AVCaptureDevice.default(for: .video) else {
            throw VideoError.setup
        }
        guard let deviceInput = try? AVCaptureDeviceInput(device: device) else {
            throw VideoError.setup
        }
        if session.canAddInput(deviceInput) {
            session.addInput(deviceInput)
        }
    }

    private func setupOutputDevice() {
        let deviceOutput = AVCaptureVideoDataOutput()
        deviceOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: NSNumber(value: kCVPixelFormatType_32BGRA)]
        deviceOutput.alwaysDiscardsLateVideoFrames = true
        deviceOutput.setSampleBufferDelegate(self, queue: queue)
        if session.canAddOutput(deviceOutput) {
            session.addOutput(deviceOutput)
        }
    }
}

extension CameraController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        handler?(pixelBuffer)
    }
}
