//
//  AudioController.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/13/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import AudioKitUI
import AudioKit

final class AudioController {
    private let generator: Generator
    private var preset: Preset?
    private let fileController: AppFileController
    private var mixer: AKMixer?
    private var recorder: AKNodeRecorder?
    private var player: AKAudioPlayer?

    init(generator: Generator = Generator(),
         fileController: AppFileController = AppFileController()) {
        self.generator = generator
        self.fileController = fileController
    }

    func start() throws {
        AKAudioFile.cleanTempDirectory()
        try AKSettings.setSession(category: .playAndRecord)
        self.mixer = AKMixer(generator.node)
        guard let recorder = try? AKNodeRecorder(node: mixer) else {
            throw AudioError.record
        }
        self.recorder = recorder
        AudioKit.output = AKMixer(mixer)
        try AudioKit.start()
    }
    
    func start(with recording: Recording, callback: (() -> Void)? = nil) throws {
        try AKSettings.setSession(category: .playAndRecord)
        guard let fileUrl = URL(string: recording.path) else {
            callback?()
            return
        }
        let file = try AKAudioFile(readFileName: fileUrl.lastPathComponent, baseDir: .documents)
        player = try AKAudioPlayer(file: file) {
            callback?()
        }
        AudioKit.output = AKMixer(player)
        try AudioKit.start()
        player?.start()
    }

    func stop() {
        AudioKit.disconnectAllInputs()
        player?.stop()
        try? AudioKit.stop()
    }

    func startRecord() throws {
        try recorder?.record()
    }

    func stopRecord() {
        if let audioFile = recorder?.audioFile {
            let fileName = createFileName(audioFile.fileName)
            fileController.export(audioFile: audioFile, presetName: fileName)
        }
        recorder?.stop()
    }
    
    func feed(data: [AnyHashable:Any]) {
        generator.feed(data: data)
    }
    
    private func createFileName(_ defaultValue: String) -> String {
        guard let presetName = self.preset?.name else {
            return defaultValue
        }
        return presetName
    }
}

extension AudioController: Configurable {
    func configure(with preset: Preset) {
        self.preset = preset
        generator.configure(with: preset)
    }
}
