//
//  ModelController.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 01/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class PresetModelController: ModelController {

    func insert(_ model: Preset) {
        let realm = getRealm()
        try! realm.write {
            let object = PresetObject(model)
            realm.add(object, update: true)
        }
    }

    func fetchAll() -> [Preset] {
        let realm = getRealm()
        return realm
            .objects(PresetObject.self)
            .sorted(byKeyPath: "name")
            .map(Preset.init)
    }
}
