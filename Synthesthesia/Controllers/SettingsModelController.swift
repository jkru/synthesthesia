//
//  SettingsModelController.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/6/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

final class SettingsModelController: ModelController {

    func update(plotType: PlotType) {
        let realm = getRealm()
        if let settings = realm.object(ofType: SettingsObject.self, forPrimaryKey: SettingsObject.key) {
            try! realm.write {
                settings.plotType = plotType.rawValue
            }
        } else {
            let object = SettingsObject()
            try! realm.write {
                object.plotType = plotType.rawValue
                realm.add(object)
            }
        }
    }

    func fetch() -> Settings {
        let realm = getRealm()
        if let settings = realm.object(ofType: SettingsObject.self, forPrimaryKey: SettingsObject.key) {
            return Settings(settings)
        }
        let object = SettingsObject()
        try! realm.write {
            realm.add(object)
        }
        return Settings(object)
    }
}
