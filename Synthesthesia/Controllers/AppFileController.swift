//
//  AppFileController.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/14/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import AudioKit

final class AppFileController: ModelController {
    
    func export(audioFile: AKAudioFile, presetName: String, completion: ((Error) -> Void)? = nil) {
        audioFile.exportAsynchronously(name: audioFile.fileName, baseDir: .documents, exportFormat: .m4a) { file, error in
            if let file = file {
                self.save(path: file.url.path, presetName: presetName)
            } else if let error = error {
                completion?(error)
            }
        }
    }
    
    func save(path: String, presetName: String) {
        let realm = getRealm()
        try! realm.write {
            let object = RecordingObject(path: path, presetName: presetName)
            realm.add(object, update: true)
        }
    }
    
    func fetchRecording(by id: String) -> RecordingObject? {
        let realm = getRealm()
        return realm.object(ofType: RecordingObject.self, forPrimaryKey: id)
    }
    
    func fetchRecordings() -> [Recording] {
        let realm = getRealm()
        return realm
            .objects(RecordingObject.self)
            .sorted(byKeyPath: "date", ascending: false)
            .map(Recording.init)
    }
    
    func rename(recording: Recording, newName: String) {
        guard let object = fetchRecording(by: recording.id) else {
            return
        }
        let realm = getRealm()
        try! realm.write {
            object.name = newName
        }
    }
    
    func delete(recording: Recording) {
        try? FileManager.default.removeItem(atPath: recording.path)
        guard let object = fetchRecording(by: recording.id) else {
            return
        }
        let realm = getRealm()
        try! realm.write {
            realm.delete(object)
        }
    }
}
