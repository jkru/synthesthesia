//
//  Model.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/5/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class ModelController {
    private let configuration: Realm.Configuration

    init(configuration: Realm.Configuration = Configuration.defaultRealm()) {
        self.configuration = configuration
    }

    func getRealm() -> Realm {
        return try! Realm(configuration: configuration)
    }
}
