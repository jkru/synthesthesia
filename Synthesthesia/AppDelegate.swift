//
//  AppDelegate.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 1/31/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window?.rootViewController = UITabBarController.default
        return true
    }
}
