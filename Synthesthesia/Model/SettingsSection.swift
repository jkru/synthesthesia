//
//  SettingsSection.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 05/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

enum SettingsSection: Int {
    case plotType = 0
    case appInfo
}

extension SettingsSection {
    static let items: [SettingsSection] = [
        .plotType,
        .appInfo
    ]
}

extension SettingsSection {
    init(section: Int) {
        self.init(rawValue: section)!
    }
}

extension SettingsSection {
    var title: String {
        switch self {
        case .plotType:
            return "Camera plot type"
        case .appInfo:
            return "App info"
        }
    }
}

extension SettingsSection {
    var height: CGFloat {
        switch self {
        case .plotType:
            return 66
        case .appInfo:
            return UITableViewAutomaticDimension
        }
    }
}
