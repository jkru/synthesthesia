//
//  SettingsObject.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/6/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class SettingsObject: Object {
    @objc private(set) dynamic var id: String = SettingsObject.key
    @objc dynamic var plotType: Int = 0

    override static func primaryKey() -> String? {
        return "id"
    }
}

extension SettingsObject {
    convenience init(_ model: Settings) {
        self.init()
        self.plotType = model.plotType.rawValue
    }
}

extension SettingsObject {
    static let key = "settings"
}
