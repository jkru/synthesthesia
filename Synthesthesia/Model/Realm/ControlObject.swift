//
//  ControlObject.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 01/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class ControlObject: Object {
    @objc private(set) dynamic var id: Int = 0
    @objc private(set) dynamic var name: String = ""
    @objc private(set) dynamic var value: Double = 0
    let mappingTypes = List<String>()
}

extension ControlObject {
    convenience init(_ model: Control) {
        self.init()
        self.id = model.id
        self.name = model.name
        self.value = model.value
        let mappingTypes = model.mappingTypes.map { $0.encoderValue }
        self.mappingTypes.append(objectsIn: mappingTypes)
    }
}
