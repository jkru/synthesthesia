//
//  PresetObject.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 01/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class PresetObject: Object {
    @objc private(set) dynamic var version: Int = 0
    @objc private(set) dynamic var name: String = ""
    @objc private(set) dynamic var desc: String? = nil
    @objc private(set) dynamic var sporth: String = ""
    let controls = List<ControlObject>()

    override static func primaryKey() -> String? {
        return "name"
    }
}

extension PresetObject {
    convenience init(_ model: Preset) {
        self.init()
        self.version = model.version
        self.name = model.name
        self.desc = model.description
        self.sporth = model.sporth
        let controls = model.controls.map(ControlObject.init)
        self.controls.append(objectsIn: controls)
    }
}
