//
//  FileObject.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 03/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import AudioKit
import Realm
import RealmSwift

final class RecordingObject: Object {
    @objc private(set) dynamic var id: String = ""
    @objc dynamic var presetName: String = ""
    @objc dynamic var name: String = ""
    @objc private(set) dynamic var date: Date = Date()
    @objc private(set) dynamic var path: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension RecordingObject {
    convenience init(_ model: Recording) {
        self.init()
        self.id = model.id
        self.name = model.name
        self.date = model.date
        self.path = model.path
    }
    
    convenience init(path: String, presetName: String) {
        self.init()
        self.id = "\(Date().ticks)"
        self.name = presetName
        self.presetName = presetName
        self.date = Date()
        self.path = path
    }
}
