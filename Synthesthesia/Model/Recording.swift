//
//  Recording.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 03/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

struct Recording {
    let id: String
    let name: String
    let presetName: String
    let date: Date
    let path: String
}

extension Recording: Equatable {
    static func == (lhs: Recording, rhs: Recording) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Recording {
    init(_ model: RecordingObject) {
        self.init(id: model.id,
                  name: model.name,
                  presetName: model.presetName,
                  date: model.date,
                  path: model.path)
    }
}
