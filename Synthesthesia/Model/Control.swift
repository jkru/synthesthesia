//
//  Control.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 02/02/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

struct Control: Codable {
    var id: Int
    var name: String
    var value: Double
    var mappingTypes: [MappingType]
}

extension Control {
    init(_ model: ControlObject) {
        let mappingTypes = model.mappingTypes.map(MappingType.init)
        self.init(id: model.id,
                  name: model.name,
                  value: model.value,
                  mappingTypes: Array(mappingTypes))
    }
}

extension Control: Equatable {
    static func == (lhs: Control, rhs: Control) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Control: Hashable {
    var hashValue: Int {
        return id
    }
}
