//
//  Settings.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 3/6/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

struct Settings {
    let plotType: PlotType
}

extension Settings {
    init(_  model: SettingsObject) {
        self.plotType = PlotType(rawValue: model.plotType) ?? .none
    }
}
