//
//  PlotType.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 05/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

enum PlotType: Int {
    case none
    case waveform
}
