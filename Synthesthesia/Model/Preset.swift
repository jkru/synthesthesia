//
//  Preset.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 02/02/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import Yams

struct Preset: Codable {
    var version: Int
    var name: String
    var description: String?
    var sporth: String
    var controls: [Control]
}

extension Preset {
    init(_ model: PresetObject) {
        let controls = model.controls.map(Control.init)
        self.init(version: model.version,
                  name: model.name,
                  description: model.desc,
                  sporth: model.sporth,
                  controls: Array(controls))
    }
}

extension Preset {
    func yaml() -> String? {
        let encoder = YAMLEncoder()
        return try? encoder.encode(self)
    }
}
