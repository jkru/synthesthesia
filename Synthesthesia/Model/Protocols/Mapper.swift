//
//  Mapper.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 11/02/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

protocol Mapper: Configurable {
    func map(data: [String:Double]) -> [Control]
}
