//
//  Configurable.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 11/02/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

protocol Configurable {
    mutating func configure(with preset: Preset)
}
