//
//  VideoError.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/9/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

enum VideoError: Error {
    case setup
}
