//
//  AudioError.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/13/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

enum AudioError: Error {
    case record
}
