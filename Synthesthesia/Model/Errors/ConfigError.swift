//
//  ConfigError.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/8/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

enum ConfigError: Error {
    case fileNotFound
    case cannotLoadFile
}
