//
//  MappingItem.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/9/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

/// Type of mapping
///
/// - category: string category
/// - allUnassigned: represents all other unassigned categories
enum MappingType {
    case category(String)
    case allUnassigned
}

extension MappingType {
    var encoderValue: String {
        switch self {
        case .allUnassigned:
            return "all_unassigned"
        case .category(let categoryName):
            return categoryName
        }
    }
}

extension MappingType {
    init(_ string: String) {
        switch string {
        case MappingType.allUnassigned.encoderValue:
            self = .allUnassigned
        default:
            self = .category(string)
        }
    }
}

extension MappingType: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let value = try container.decode(String.self)
        self = MappingType(value)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.encoderValue)
    }
}

extension MappingType: Equatable {
    static func == (lhs: MappingType, rhs: MappingType) -> Bool {
        switch (lhs, rhs) {
        case (.allUnassigned, .allUnassigned):
            return true
        case (.category(let lcat), .category(let rcat)):
            return lcat == rcat
        default:
            return false
        }
    }
}

extension MappingType: Hashable {
    var hashValue: Int {
        switch self {
        case .category(let categoryName):
            return categoryName.hashValue
        case .allUnassigned:
            return 0
        }
    }
}

extension RandomAccessCollection where Iterator.Element == Mapping {
    func filter(by type: MappingType) -> [Mapping] {
        return self.filter { $0.type == type }
    }

    func filterAllUnassigned() -> [Mapping] {
        return filter(by: .allUnassigned)
    }
}
