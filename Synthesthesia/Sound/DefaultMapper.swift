//
//  Mapper.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 02/02/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation

/// Responsible for mapping and updating new values to controls
final class DefaultMapper {
    private var mapping: [Mapping]

    init() {
        self.mapping = []
    }

    private func updateControls(ofType type: MappingType, with value: Double) -> [Control] {
        var mappingForType = mapping.filter { $0.type == type }
        if mappingForType.isEmpty {
            mappingForType = mapping.filter { $0.type == .allUnassigned }
        }
        return mappingForType.map { item in
            var control = item.control
            control.value = value
            return control
        }
    }

    private func assign(mappingTypes: [MappingType], to control: Control) -> [Mapping] {
        var result = [Mapping]()
        for type in mappingTypes {
            result.append(Mapping(type: type, control: control))
        }
        return result
    }
}

extension DefaultMapper: Configurable {
    func configure(with config: Preset) {
        mapping = []
        for control in config.controls {
            let newMapping = assign(mappingTypes: control.mappingTypes, to: control)
            mapping.append(contentsOf: newMapping)
        }
    }
}

extension DefaultMapper: Mapper {
    func map(data: [String:Double]) -> [Control] {
        var result = [Control]()
        for (key, value) in data {
            let newControls = updateControls(ofType: MappingType(key), with: value)
            result.append(contentsOf: newControls)
        }
        return result
    }
}
