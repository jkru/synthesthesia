//
//  Generator.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 02/02/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import AudioKit

/// Responsible for generating and manipulating sound
final class Generator {
    private var operationGenerator: AKOperationGenerator
    private var mapper: Mapper

    init(operation: AKOperationGenerator = AKOperationGenerator(sporth: ""),
         mapper: Mapper = DefaultMapper()) {
        self.operationGenerator = operation
        self.mapper = mapper
    }

    func feed(data: [AnyHashable:Any]) {
        guard let data = data as? [String:Double] else {
            return
        }
        let controls = mapper.map(data: data)
        applyToOperation(controls: controls)
    }

    private func applyToOperation(controls: [Control]) {
        for control in controls {
            operationGenerator.parameters[control.id] = control.value
        }
    }

    var node: AKNode {
        return operationGenerator
    }
}

extension Generator: Configurable {
    func configure(with preset: Preset) {
        mapper.configure(with: preset)
        operationGenerator.sporth = preset.sporth
        applyToOperation(controls: preset.controls)
    }
}
