//
//  PreviewViewController.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 25/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import UIKit

final class PreviewViewController: UIViewController {
    private var preset: Preset!
    @IBOutlet var textLabel: UILabel!

    static func instantiate(preset: Preset) -> PreviewViewController {
        let viewController: PreviewViewController = UIViewController.load(from: .preview, with: .preview)
        viewController.preset = preset
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = preset.name
        textLabel.text = preset.yaml() ?? ""
    }
}
