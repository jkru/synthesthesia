//
//  CameraViewController.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 2/14/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import AudioKit
import AudioKitUI
import UIKit

final class CameraViewController: UIViewController {
    @IBOutlet var overlayView: UIView!
    @IBOutlet var recordButton: UIButton!
    @IBOutlet var presetButton: UIButton!
    @IBOutlet var toolbar: UIToolbar!
    private var cameraController: CameraController!
    private var audioController: AudioController!
    private var tensorflowController: TensorflowController!
    private var settingsController: SettingsModelController!
    private var isRecording = false
    private var preset: Preset!

    static func instantiate(preset: Preset,
                            cameraController: CameraController = CameraController(),
                            audioController: AudioController = AudioController(),
                            tensorflowController: TensorflowController = TensorflowController(),
                            settingsController: SettingsModelController = SettingsModelController()) -> CameraViewController {
        let viewController: CameraViewController = UIViewController.load(from: .camera, with: .camera)
        viewController.preset = preset
        viewController.cameraController = cameraController
        viewController.audioController = audioController
        viewController.tensorflowController = tensorflowController
        viewController.settingsController = settingsController
        return viewController
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configure()
    }

    private func configure() {
        audioController.configure(with: preset)
        tensorflowController.initalize()
        cameraController.handler = { [weak self] frame in
            self?.handle(frame: frame)
        }
        
        DispatchQueue.main.async {
            try? self.cameraController.start(for: self.overlayView)
            try? self.audioController.start()
            self.addPlotView()
         }
    }
    
    private func addPlotView() {
        let plotType = settingsController.fetch().plotType
        switch plotType {
        case .waveform:
            let plotView = UIView.plotView(inView: view)
            overlayView.addSubview(plotView)
        case .none:
            ()
        }
    }

    private func setupLayout() {
        toggleButtonLayout()
        recordButton.circle()
        toolbar.transparent()
        presetButton.setTitle(preset.name, for: .normal)
    }

    private func animateButton() {
        UIView.animate(withDuration: .buttonAnimationTime,
            delay: 0.0,
            options: [.curveEaseInOut, .autoreverse, .repeat, .allowUserInteraction],
            animations: { [weak self] in self?.recordButton.backgroundColor = .white },
            completion: { [weak self] _ in self?.recordButton.backgroundColor = .red }
        )
    }
    
    private func toggleButtonLayout() {
        if isRecording {
            animateButton()
        } else {
            view.layer.removeAllAnimations()
            recordButton.backgroundColor = .red
            recordButton.layer.borderColor = UIColor.white.cgColor
        }
    }

    private func handle(frame: CVImageBuffer) {
        tensorflowController.runModel(onFrame: frame) { [weak self] data in
            self?.audioController.feed(data: data)
        }
    }
    
    @IBAction func didTapRecordButton(_ sender: Any) {
        isRecording = !isRecording
        toggleButtonLayout()
        if isRecording {
            try! audioController.startRecord()
        } else {
            audioController.stopRecord()
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        if isRecording {
            didTapRecordButton(sender)
        }
        DispatchQueue.main.async {
            self.cameraController.stop()
            self.audioController.stop()
        }
        dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapInfoButton(_ sender: Any) {
        print("didTapInfoButton")
    }

    @IBAction func didTapPresetButton(_ sender: Any) {
        print("didTapPresetButton")
    }
}
