//
//  SettingsViewController.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 03/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import UIKit

final class SettingsViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    private var controller: SettingsModelController!
    
    static func instantiate(controller: SettingsModelController = SettingsModelController()) -> SettingsViewController {
        let viewController: SettingsViewController = UIViewController.load(from: .settings, with: .settings)
        viewController.controller = controller
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.dataSource = self
        tableView.delegate = self
        navigationItem.title = "Settings"
    }

    private func getPlotType() -> PlotType {
        return controller.fetch().plotType
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return SettingsSection.items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = SettingsSection(section: indexPath.section)
        return section.height
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = SettingsSection(section: section)
        return section.title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = SettingsSection(section: indexPath.section)
        switch section {
        case .plotType:
            let cell: PlotTypeTableViewCell = tableView.cell(id: .plotType, for: indexPath)
            cell.configure(with: getPlotType())
            cell.didTapPlotType = { [weak self] type in
                self?.controller.update(plotType: type)
            }
            return cell
        case .appInfo:
            return tableView.cell(id: .appInfo, for: indexPath)
        }
    }
}
