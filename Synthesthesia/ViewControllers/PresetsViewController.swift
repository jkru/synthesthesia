//
//  ViewController.swift
//  Synthesthesia
//
//  Created by Krukowski, Jan on 1/31/18.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import UIKit
import AudioKit

final class PresetsViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    private var statusBarShouldBeHidden = false
    private var presets = [Preset]()
    private var modelController: PresetModelController!

    static func instantiate(modelController: PresetModelController = PresetModelController()) -> PresetsViewController {
        let viewController: PresetsViewController = UIViewController.load(from: .presets, with: .presets)
        viewController.modelController = modelController
        return viewController
    }

    override var prefersStatusBarHidden: Bool {
        return statusBarShouldBeHidden
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Presets"
        presets = modelController.fetchAll()
        tableView.dataSource = self
        tableView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateStatusBar(isHidden: false)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    private func animateStatusBar(isHidden: Bool) {
        statusBarShouldBeHidden = isHidden
        UIView.animate(withDuration: .statusBarAnimationTime(isHidden)) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
        navigationController?.setNavigationBarHidden(isHidden, animated: true)
    }

    private func pushCamera(with preset: Preset) {
        let camerViewController = CameraViewController.instantiate(preset: preset)
        animateStatusBar(isHidden: true)
        navigationController?.present(camerViewController, animated: true, completion: nil)
    }
    
    private func preview(at indexPath: IndexPath) {
        let preset = presets[indexPath.row]
        let viewController = PreviewViewController.instantiate(preset: preset)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension PresetsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presets.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PresetTableViewCell = tableView.cell(id: .preset, for: indexPath)
        cell.configure(with: presets[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let preset = presets[indexPath.row]
        pushCamera(with: preset)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let previewAction = UIContextualAction(style: .normal, title: "Preview") { _, _, callback in
            callback(true)
            self.preview(at: indexPath)
        }
        previewAction.backgroundColor = .orange
        let configuration = UISwipeActionsConfiguration(actions: [previewAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}
