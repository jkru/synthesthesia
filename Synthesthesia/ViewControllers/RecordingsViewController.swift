//
//  RecordingsViewController.swift
//  Synthesthesia
//
//  Created by Janek Krukowski on 03/03/2018.
//  Copyright © 2018 Krukowski, Jan. All rights reserved.
//

import Foundation
import UIKit

final class RecordingsViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    private var appFileController: AppFileController!
    private var audioController: AudioController!
    private var recordings = [Recording]()
    private var playingRecording: Recording?
    
    static func instantiate(appFileController: AppFileController = AppFileController(),
                            audioController: AudioController = AudioController()) -> RecordingsViewController {
        let viewController: RecordingsViewController = UIViewController.load(from: .recordings, with: .recordings)
        viewController.appFileController = appFileController
        viewController.audioController = audioController
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "My recordings"
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        recordings = appFileController.fetchRecordings()
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        playingRecording = nil
        audioController.stop()
    }
    
    private func delete(at indexPath: IndexPath) {
        appFileController.delete(recording: recordings[indexPath.row])
        recordings = appFileController.fetchRecordings()
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }

    private func rename(recording: Recording, newName: String, indexPath: IndexPath) {
        appFileController.rename(recording: recording, newName: newName)
        recordings = self.appFileController.fetchRecordings()
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    private func rename(at indexPath: IndexPath) {
        let recording = recordings[indexPath.row]
        let altertController = UIAlertController.renameAlertController(name: recording.name) { [weak self] newName in
            self?.rename(recording: recording, newName: newName, indexPath: indexPath)
        }
        present(altertController, animated: true)
    }
    
    private func toggle(isPlaying: Bool, recording: Recording) {
        if isPlaying {
            playingRecording = recording
            audioController.stop()
            try! audioController.start(with: recording) { [weak self] in
                self?.toggle(isPlaying: false, recording: recording)
            }
        } else {
            playingRecording = nil
            audioController.stop()
        }
        tableView.reloadSections([0], with: .automatic)
    }
}

extension RecordingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordings.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RecordingTableViewCell = tableView.cell(id: .recording, for: indexPath)
        let recording = recordings[indexPath.row]
        cell.configure(with: recording, isPlaying: recording == playingRecording)
        cell.didTap = { [weak self] isPlaying, recording in
            self?.toggle(isPlaying: isPlaying, recording: recording)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, callback in
            callback(true)
            self.delete(at: indexPath)
        }
        let renameAction = UIContextualAction(style: .normal, title: "Rename") { _, _, callback in
            callback(true)
            self.rename(at: indexPath)
        }
        deleteAction.backgroundColor = .red
        renameAction.backgroundColor = .orange
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, renameAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}
